<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    #[Route('/api', name: 'app_api')]
    public function index(): Response
    {

        $QA = [
            [
                "question" => "Question A", 
                "answer" => "Answer A"
            ],
            [
                "question" => "Question B", 
                "answer" => "Answer B"
            ],
            [
                "question" => "Question C", 
                "answer" => "Answer C"
            ],
            [
                "question" => "Question D", 
                "answer" => "Answer D"
            ]
        ] ; 

        return $this->json($QA);
    }
}
